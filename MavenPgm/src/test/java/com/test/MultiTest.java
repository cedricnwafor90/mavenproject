package com.test;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class MultiTest {

	WebDriver driver;

	@BeforeClass
	public void Initalization() {
		System.setProperty("webdriver.chrome.driver", "C:\\Users\\cedri\\Desktop\\SeleniumJars\\chromedriver.exe");

		driver = new ChromeDriver();

		driver.get("https://www.amazon.com");

		driver.manage().window().maximize(); // maximize the window

		System.out.println(driver.getTitle()); //
	}

	@Test
	public void SelectDropDown() {

		WebElement selbox = driver.findElement(By.id("searchDropdownBox"));

		Select sel = new Select(selbox);

		sel.selectByIndex(5);

		sel.selectByValue("search-alias=electronics-intl-ship");

		sel.selectByVisibleText("Deals");

	}

	@Test( dependsOnMethods = "SelectDropDown" )
	public void AdvanceDropDown() {

		WebElement selbox = driver.findElement(By.id("searchDropdownBox"));

		Select sel = new Select(selbox);

		List<WebElement> li = sel.getOptions();

		System.out.println(li.size());

		for (int i = 0; i < li.size(); i++) {

			// li.get(i).click();
			// System.out.println(li.get(i).getText());

			if (li.get(i).getText().equalsIgnoreCase("Electronics")) {
				li.get(i).click();
			}
		}

	}
	
	//running test
	
	
	@Test
	public void Test1()
	{
		System.out.println("Execute Test1 Method");
	}
	
	@Test(dependsOnMethods = "AdvanceDropDown")
	public void Test2()
	{
		System.out.println("Execute Test2 method");
	}

	@AfterClass
	public void CleanUp() {
		driver.close();
	}

}
